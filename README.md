# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

After cloning the project from git - take the steps below to run the project
- npm install - installs all the necessary missing modules
- npm start - Builds up the project, compile the typescript and opens a browser on localhost 3000

### USAGE
- Once the page loads, you should be able to see a list of photos with thumbnail, title, current date
- The rows are organized in white and some light shade of gray
- If you hover over the titles, they should show some border
- If you click on a photo title, the border changes into an input element allowing you to edit the title
- You can edit the title, confirm update and reset to previous state
- If you scroll down, you should be able to see the more photos loading to the page

### TESTING
- Unfortunately, I wasn't able to add tests for this one

### CHALLENGES
- With typscript I'm a bit rusty - would have to take refresher courses to be up to speed

### SCREENSHOTS
![SCREENSHOT](https://bitbucket.org/repo/MbrpoXB/images/2129785316-Screenshot%202022-02-11%20at%2002.08.01.png)
