import { combineReducers } from '@reduxjs/toolkit'
import photos from './containers/photos/reducer'

const rootReducer = combineReducers({ photos })

export default rootReducer

export type RootState = ReturnType<typeof rootReducer>
