declare global {
  namespace NodeJS {
    interface ProcessEnv {
      REACT_APP_API_URL: string;
    }
  }
}

interface envInterface {
  [key: string]: any;
  REACT_APP_API_URL: string;
}

const envVars: envInterface = process.env

const config = (env: string): any => {
  const key: string = `REACT_APP_${env}`
  if (!envVars[key]) throw new Error('no config found for ' + key)
  return envVars[key]
}

export default config
