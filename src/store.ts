import { configureStore } from '@reduxjs/toolkit'
import combinedReducers from './combined-reducers'

const store = configureStore({
  reducer: combinedReducers
})

export default store
