import React from 'react';
import { Provider } from 'react-redux'
import './App.css';
import store from './store'
import Photos from './containers/photos'

function App() {
  return (
    <Provider store={store}>
      <header className="App-header">Photos API</header>
      <Photos />
    </Provider>
  );
}

export default App;
