import { Dispatch } from 'redux'
import { getPhotos } from '../../api/photos'
import { RootState } from '../../combined-reducers'
import {
  InitialState,
  PhotoInterface,
  SET_PHOTOS,
  SET_PREVIOUS_PHOTOS,
  LOADING_PHOTOS,
  LOADING_MORE,
  NO_MORE_PHOTOS,
  dispatchTypes
} from './types'

const CHUNK = 20

const initialState: InitialState = {
  loading: false,
  photos: [],
  previousPhotos: [],
  noMorePhotos: false,
  loadingMore: false
}

const photosReducer = (state :InitialState = initialState, action: dispatchTypes): InitialState => {
  switch (action.type) {
    case SET_PHOTOS:
      return { ...state, photos: action.photos }
    case LOADING_PHOTOS:
      return { ...state, loading: action.isLoading }
    case LOADING_MORE:
      return { ...state, loadingMore: action.loadingMore }
    case SET_PREVIOUS_PHOTOS:
      return { ...state, previousPhotos: action.previousPhotos }
  }
  return state
}

export const retrievePhotos = () => async (dispatch: Dispatch<dispatchTypes>, getState: () => RootState) => {
  if (getState().photos.noMorePhotos) return
  dispatch({ type: LOADING_PHOTOS, isLoading: true})
  const photos = await getPhotos(`/photos?_start=0&_end=${CHUNK}`)
  dispatch({ type: SET_PHOTOS, photos })
  dispatch({ type: LOADING_PHOTOS, isLoading: false})
}

export const retrieveMore = () => async (dispatch: Dispatch<dispatchTypes>, getState: () => RootState) => {
  if (getState().photos.noMorePhotos) return
  dispatch({ type: LOADING_MORE, loadingMore: true})
  const previousPhotos = getState().photos.photos
  const finalPhoto = getState().photos.photos[previousPhotos.length - 1]
  let start: number = finalPhoto.id
  let end: number = start + CHUNK
  const photos = await getPhotos(`/photos?_start=${start}&_end=${end}`)
  photos.length > 0
    ? dispatch({ type: SET_PHOTOS, photos: [...previousPhotos, ...photos] })
    : dispatch({ type: NO_MORE_PHOTOS })
  dispatch({ type: LOADING_MORE, loadingMore: false})
}

export const updatePhotos = (photos: PhotoInterface[]) => (dispatch: Dispatch<dispatchTypes>, getState: () => RootState) => {
  const previousPhotos: PhotoInterface[] = getState().photos.photos
  dispatch({ type: SET_PREVIOUS_PHOTOS, previousPhotos })
  dispatch({ type: SET_PHOTOS, photos })
}

export const resetPhotos = () => (dispatch: Dispatch<dispatchTypes>, getState: () => RootState) => {
  const photos = getState().photos.previousPhotos
  photos.length > 0 && dispatch({ type: SET_PHOTOS, photos })
}

export default photosReducer
