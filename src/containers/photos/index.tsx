import { useEffect } from 'react';
import { useDispatch } from 'react-redux'
import PhotosComponent from '../../components/photos/index'
import { retrievePhotos, updatePhotos, resetPhotos, retrieveMore } from './reducer'
import { PhotoInterface } from './types'

export default function Photos () {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(retrievePhotos())
  }, [dispatch])

  return (
    <PhotosComponent
      retrievePhotos={() => dispatch(retrieveMore())}
      updatePhotos={(photos: PhotoInterface[]) => dispatch(updatePhotos(photos))}
      resetPhotos={() => dispatch(resetPhotos())}
    />
  )
}
