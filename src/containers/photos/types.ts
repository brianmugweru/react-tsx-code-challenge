export interface PhotoInterface {
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

export interface InitialState {
  loading: boolean;
  photos: PhotoInterface[];
  previousPhotos: PhotoInterface[];
  noMorePhotos: boolean;
  loadingMore: boolean;
}

export const SET_PHOTOS = 'SET_PHOTOS'
export const SET_PREVIOUS_PHOTOS = 'SET_PREVIOUS_PHOTOS'
export const LOADING_PHOTOS = 'LOADING_PHOTOS'
export const LOADING_MORE = 'LOADING_MORE'
export const NO_MORE_PHOTOS = 'NO_MORE_PHOTOS'

export interface SetPhotos {
  type: typeof SET_PHOTOS,
  photos: PhotoInterface[]
}

export interface SetPreviousPhotos {
  type: typeof SET_PREVIOUS_PHOTOS,
  previousPhotos: PhotoInterface[]
}

export interface SetLoadingPhotos {
  type: typeof LOADING_PHOTOS;
  isLoading: boolean;
}

export interface SetLoadingMore {
  type: typeof LOADING_MORE;
  loadingMore: boolean;
}

export interface SetNoMorePhotos {
  type: typeof NO_MORE_PHOTOS
}

export type dispatchTypes = SetPhotos | SetLoadingPhotos | SetPreviousPhotos | SetNoMorePhotos | SetLoadingMore
