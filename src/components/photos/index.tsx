import { FC, useState, useEffect, Fragment } from 'react'
import { useSelector } from 'react-redux'
import ReactLoading from 'react-loading'
import Photo from '../photo'
import './index.scss'
import { RootState } from '../../combined-reducers'
import { PhotoInterface } from '../../containers/photos/types'

interface Props {
  updatePhotos: (photos: PhotoInterface[]) => void,
  retrievePhotos: () => void,
  resetPhotos: () => void
}

const Photos: FC<Props> = ({ updatePhotos, resetPhotos, retrievePhotos }) => {
  const photos = useSelector((state: RootState): PhotoInterface[] => state.photos.photos)
  const loading = useSelector((state: RootState): boolean => state.photos.loading)
  const loadingMore = useSelector((state: RootState): boolean => state.photos.loadingMore)
  const [editableId, setEditableId] = useState<number>(0)
  const [shouldUpdate, setShouldUpdate] = useState(true)
  const [shouldReset, setShouldReset] = useState(true)
  const [items, setItems] = useState<PhotoInterface[]>(photos)

  useEffect(() => {
    setItems(photos)
    setShouldUpdate(true)
  }, [photos])


  const editPhotoTitle = (photoId: number, photoTitle: string) => {
    const updatedPhotos = items.map((photo: PhotoInterface) => ({
      ...photo.id === photoId ? ({
        ...photo,
        title: photoTitle
      }) : photo
    }))

    setItems(updatedPhotos)

    const lengthOfChangedItems = updatedPhotos.map((photo: PhotoInterface) => {
      return photo.title  === photos.find((p: PhotoInterface) => photo.id === p.id)?.title
    }).filter((titleChanged: boolean) => titleChanged === false).length

    setShouldUpdate(lengthOfChangedItems === 0)
    setShouldReset(lengthOfChangedItems === 0)
  }

  const resetComponentPhotos = () => {
    const lengthOfChangedItems = photos.map((photo: PhotoInterface) => {
      return photo.title  === items.find((p: PhotoInterface) => photo.id === p.id)?.title
    }).filter((titleChanged: boolean) => titleChanged === false).length

    lengthOfChangedItems === 0 ? resetPhotos() : setItems(photos)
    setShouldReset(lengthOfChangedItems === 0)
  }

  const handleScroll = (event: any) => {
    const target = event.target
    if (target.scrollHeight - target.scrollTop === target.clientHeight) {
      retrievePhotos()
    }
  }

  return (
    <div className="photos" onScroll={handleScroll}>
      {loading
        ? (
          <div style={{display: 'flex', flexBasis: '20px'}}>
            {
              [...Array(15)].map(() => (
                <ReactLoading
                  type={'spin'}
                  color={'#1c2951'}
                  height={0}
                  width={70}
                  className={'fbasis'}
                />
              ))
            }
          </div>
        )
        : (
          <Fragment>
            <div className={'buttons'}>
              <button onClick={() => updatePhotos(items)} disabled={shouldUpdate}>Confirm Update</button>
              <button onClick={resetComponentPhotos} disabled={shouldReset}>Reset Photos</button>
            </div>
            {items.map((photo: PhotoInterface, index: number) => (
              <Photo
                photo={photo}
                key={photo.id}
                isEditable={photo.id === editableId}
                isEven={photo.id % 2 === 0}
                setEditableId={setEditableId}
                editPhotoTitle={editPhotoTitle}
              />
            ))}
          </Fragment>
        )
      }
      {loadingMore && (
        <div style={{display: 'flex', flexBasis: '20px'}}>
          {
            [...Array(15)].map(() => (
              <ReactLoading
                type={'spin'}
                color={'#1c2951'}
                height={0}
                width={70}
                className={'fbasis'}
              />
            ))
          }
        </div>
      )}
    </div>
  )
}

export default Photos
