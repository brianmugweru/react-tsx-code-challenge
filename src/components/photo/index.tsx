import { FC, useState, useRef } from 'react'
import moment from 'moment'
import './index.scss'
import { PhotoInterface } from '../../containers/photos/types'

interface Props {
  photo: PhotoInterface,
  isEven: boolean,
  isEditable: boolean,
  setEditableId: (photoId: number) => void,
  editPhotoTitle: (photoId: number, photoTitle: string) => void,
}

const Photo: FC<Props> = ({ photo, isEditable, setEditableId, isEven, editPhotoTitle }) => {
  const [inputField, setInputField] = useState(false)
  const titleRef = useRef<any>(document.createElement('div'))
  const handleKeyDown = (e: any) => {
    if (e.key === 'Enter') {
      titleRef.current.blur()
    }
  }

  return (
    <div
      className={isEven ? 'photo grayBackground' : 'photo'}
      key={photo.id}
      onMouseEnter={() => setInputField(true)}
      onMouseLeave={() => setInputField(false)}
    >
        <div><img src={photo.thumbnailUrl} alt={photo.title} /></div>
        <div>{moment(Date.now()).toString()}</div>
        <div>{photo.id}</div>
        <div>
          {isEditable
            ? <input
                value={photo.title}
                onChange={(event) => { editPhotoTitle(photo.id, event.target.value) }}
                onBlur={() => setEditableId(0)}
                name='title'
                autoFocus
                onKeyDown={handleKeyDown}
                ref={titleRef}
              />
              : <label
                  className={inputField ? 'borders' : ''}
                  onClick={() => setEditableId(photo.id)}
                >
                  {photo.title}
                </label>
          }
        </div>
    </div>
  )
}

export default Photo
