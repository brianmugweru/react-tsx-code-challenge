import axios, { Method } from 'axios'
import config from '../helpers/config'

interface Defaults {
  baseURL: string;
  headers: () => {};
}

interface requestMethods {
  get: (url: string, data?: returnType) => {}
}

const defaults: Defaults = {
  baseURL: config('API_URL'),
  headers: () => ({ 'Content-Type': 'application/json' }),
}

type returnType = object | object[] | undefined

export default function request (): requestMethods {
  const api = (method: Method, url: string, data?: returnType) => {
    return new Promise((resolve, reject) => {
      return axios({
        url,
        method,
        baseURL: defaults.baseURL,
        headers: defaults.headers(),
        params: method === 'get' ? data : undefined,
        data: method !== 'get' ? data : undefined,
      }).then(
        (response) => resolve(response.data),
        (error) => {
          const status = error?.response?.status || 503
          reject(`something is wrong ${error.response.message}: ${status}`)
        }
      )
    })
  }

  return {
    get: (...args: [string, returnType]) => api('get', ...args)
  }
}
