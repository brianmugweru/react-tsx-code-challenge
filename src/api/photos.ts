import api from './api'

export const getPhotos = (url: string): any => {
  return api.get(url)
}
